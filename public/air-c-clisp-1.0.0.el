;;; air-c-clisp.el --- Common lisp development config.  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Aimar Ibarra

;; Author: Aimar Ibarra <greenerclay@gmail.com>
;; Keywords: lisp, tools
;; Version: 1.0.0
;; Package-Requires: ((slime))
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configuration package for common lisp development.

;;; Code:

(require 'slime)

(defun air-c-clisp-run ()
  (interactive)
  (setq inferior-lisp-program (executable-find "sbcl"))
  (slime-setup))

(provide 'air-c-clisp)
;;; air-c-clisp.el ends here
