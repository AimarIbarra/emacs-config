;;; air-i-amd64-clisp.el --- Installation package for common lisp development  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  aimar

;; Author: aimar <greenerclay@gmail.com>
;; Keywords: lisp, tools
;; Version: 1.0.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package will install all the required third party dependencies
;; for common lisp development.

;;; Code:

(defun air-i-amd64-clisp-run ()
  (interactive)
  (with-current-buffer (get-buffer-create "*air-i-amd64-clisp-run log*")
    (cond
     ((eq system-type 'gnu/linux)
      (unless (executable-find "sbcl")
        (let ((url "https://sourceforge.net/projects/sbcl/files/sbcl/2.3.11/sbcl-2.3.11-x86-64-linux-binary.tar.bz2/download")
              (tarball-file "/tmp/sbcl/sbcl-2.3.11-linux.tar.bz2")
              (extracted-dir "/tmp/sbcl/"))
          
          ;; Tmp dir
          (unless (file-exists-p extracted-dir)
            (make-directory extracted-dir t)
            (message "Created /tmp/sbcl/"))
          
          ;; Download tarball and extract
          (url-copy-file url tarball-file t)
          (message (concat "Extracting contents from " tarball-file "...\n"))
          (call-process "tar" nil t nil
                        "-xjf"
                        tarball-file
                        "-C"
                        extracted-dir
                        "--strip-components=1")
          
          ;; Install binaries
          (cd "/tmp/sbcl")
          (shell-command (concat "echo " (shell-quote-argument (read-passwd "Password: "))
                                 " | sudo -S sh install.sh --prefix=/usr/local")
                         t)
          (message "Done! Cleaning up...")
          (delete-directory "/tmp/sbcl" t))))
     (t (error "System not supported.")))
    (current-buffer)))

(provide 'air-i-amd64-clisp)
;;; air-i-amd64-clisp.el ends here
